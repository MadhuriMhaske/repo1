package findMinMax;

import java.util.Scanner;

public class Main 
{

	public static void main(String[] args) 
	{
		int i,p,choice,len=0; 
		
		//  the size of array from user.
		Scanner scan =new Scanner(System.in);
		System.out.println("Enter the size of arr::");
		int size=scan.nextInt();
		//Array DEclaration
		int  [] arr=new int[size];
		FindMinMax s=new FindMinMax();
		//Choice from user.
		System.out.println("Enter the Choice::");
		choice = s.option(scan); 
		
		while(choice != 5)
		{                
            switch (choice)
            {                    
                case 1: 
                	//Number of element in array
                	try {
                	System.out.println("How many element do you want to enter?");
               
    		         len=scan.nextInt();
    		         if(len>size)
    		         {
    		        	 throw new ArrayIndexOutOfBoundsException("");
    		         }
                	}
                	catch(ArrayIndexOutOfBoundsException e)
                	{
                		System.out.println("Please enter element less than size");
                		break;
                	}
    		       System.out.println("add element");
    		       //choice to add an element to Array.
                	arr = s.addItems(arr, scan,len); 
                	System.out.println("Displaying unsorted element.....");
                	for(int k=0;k<=len-1;k++)
                	{
                		System.out.println(arr[k]);
                	}
                break;
                //to Remove an element into array
                case 2:  arr = s.removeItems(arr, scan,len);   
                break;
                //Quick Sort 
                case 3:  
                	
                	 s.quickSort(arr, 0, len-1);  
                System.out.println("\n The sorted array is: \n");  
                //Displaying Sorted Array
		        for(i=0;i<=len-1;i++)  
		        {
		        System.out.println(arr[i]);
		        }
		        
                break; 
                //Finding Min value and Max Value.
                case 4:
                	System.out.println("Finding Min and Max number");
                	 s.quickSort(arr, 0, len-1); 
                	System.out.println("Minimum number in array is::"+arr[0]);
                	System.out.println("Maximum number in array is::"+arr[len-1]);
                 
                break;
            }

            choice = s.option(scan);
        }            

	}
}

