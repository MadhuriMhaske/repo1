package findMinMax;

import java.util.Scanner;

public class FindMinMax {
	

	 //Java Program to Implement Quick Sort



	   public static int partition(int a[], int left, int right)  
		    {
		   	int pivot=a[(left+right)/2];
		   	while(left<=right)
		   	{
		   		
		   			while(a[left]<pivot)
		   				left++;
		   			while(a[right]>pivot)
		   			right--;
		   			if(left<=right)
		   			{
		   				int temp=a[left];
		   				a[left]=a[right];
		   				a[right]=temp;
		   				left++;
		   				right--;
		   			}
		   		
		   	}
		   
				return left;  
		          
		   
		        }  

		   

		   public  void quickSort(int a[], int left, int right)  
		    {  
		      
			   int index=partition(a,left,right);
			   if(left<index-1)
			   {
				   quickSort(a,left,index-1); 
			   }
			   if(index<right)
			   {
				   quickSort(a,index,right); 
			   }
			 
		    }
		    
		   //method to select option
		public int option(Scanner scan) 
		{
			System.out.println("\n");
	        System.out.println("Here are your choices: \n" + "1: Add Items " + "2: Remove Items " + "3: Sort Items " + "4: Find Min and Max");
	        System.out.println("-");
	        int choice = scan.nextInt();
	        return choice;
		} 
		//method to add Element
		public  int[] addItems(int array[], Scanner scan,int len){       
	        for(int i = 0; i <=len-1; i ++){                
	            System.out.print("Value #" + (i + 1) + ": ");                
	            array[i] = scan.nextInt();
	        }            
	        return array;
	    }
		//method to remove element
		public int[] removeItems(int[] arr, Scanner scan,int len) {
			System.out.println("Number to be removed: ");            
	        for(int i = 0; i < len; i ++){
	            System.out.println("Item removed: " + arr[i]);                
	            try {                    
	                arr[i] = scan.nextInt();
	            }               
	            catch(Exception e){                    
	                arr[len - 1] = 0;
	            }
	        }            
	        return arr;
	} 
	    }

