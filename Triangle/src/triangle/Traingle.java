package triangle;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.JOptionPane;
import javax.swing.JApplet;
import javax.swing.JFrame;


@SuppressWarnings("serial")
public class Traingle extends JApplet 
{

	public void paint(Graphics g)
	{
		int side1, side2, side3;
         //Enter the Value of three sides.
		String strside1 = JOptionPane.showInputDialog("Enter side1");
		side1 = Integer.parseInt(strside1);

		String strSide2 = JOptionPane.showInputDialog("Enter Side2: ");
		side2 = Integer.parseInt(strSide2);

		String strSide3 = JOptionPane.showInputDialog("Enter Side3: ");
		side3 = Integer.parseInt(strSide3);

		Graphics2D ga = (Graphics2D) g;
		//set the thickness of line
		ga.setStroke(new BasicStroke(5));
		ga.setColor(Color.blue);
		ga.drawString("Side A:" + side1, 30, 50);
		ga.setColor(Color.blue);
		ga.drawString("side B:" + side2, 30, 70);
		ga.setColor(Color.blue);
		ga.drawString("Side C:" + side3, 30, 90);
		if ((side1 + side2 > side3) && (side1 + side3 > side2) && (side2 + side3 > side1)) 
		{
			if (side1 < 10 || side2 < 10 || side3 < 10) 
			{
				ga.drawLine(220, ((side1 + 50) + 120), (220 + ((side3 + 50) / 2)), 120);
				ga.drawLine((220 + (side3 + 50)), ((side2 + 50) + 120), (220 + ((side3 + 50) / 2)), 120);
				ga.drawLine(220, ((side1 + 50) + 120), (side3 + 50) + 220, (side2 + 50) + 120);
			} 
			else 
			{
				ga.drawLine(220, (side1 + 120), (220 + (side3 / 2)), 120);
				ga.drawLine((220 + side3), (side2 + 120), (220 + (side3 / 2)), 120);
				ga.drawLine(220, (side1 + 120), side3 + 220, side2 + 120);
			}
		}
		//condition to check for negatives
		if (checkForNegatives(side1, side2, side3) == false) 
		{
			if (checkForTriangle(side1, side2, side3) == true)
			{
				String result = evaluate(side1, side2, side3);
				JOptionPane.showMessageDialog(null, result);
			} 
			else
				JOptionPane.showMessageDialog(null, "Refer Traingle Inequality Theorem", "Results",
				JOptionPane.PLAIN_MESSAGE);
		} 
		else
			JOptionPane.showMessageDialog(null, "Please Enter positive Sides", "Results", JOptionPane.PLAIN_MESSAGE);

	}
	
	
	
	//method to check for Negative
	private static boolean checkForNegatives(int side1, int side2, int side3) 
	{
		if (side1 <= 0 || side2 <= 0 || side3 <= 0)
			return true;
		else
			return false;
	}

	// evaluate if a triangle is physically possible 
	private static boolean checkForTriangle(int side1, int side2, int side3) 
	{
		if (side1 + side2 - side3 <= 0 || side1 + side3 - side2 <= 0 || side2 + side3 - side1 <= 0)
			return false;
		else
			return true;
	}
	//method to find the type of traingle

	private static String evaluate(int side1, int side2, int side3) 
	{
		//condition to check for Equilateral
		if (side1 == side2 && side2 == side3) 
		{
			return "Equilateral";
		}
		//condition to check for Isosceles
		if (side1 == side2 && side2 != side3 || side1 == side3 && side2 != side3 || side2 == side3 && side1 != side2)
		
		{
			return "Isosceles ";
		}
		//condition to check for Scalene
		if (side1 != side2 && side2 != side3 || side1 != side2 && side1 != side3) 
		{
			return "Scalene";
		}
		return "test value";
	}
		//init Method
	public void init() 
	{
		setBackground(Color.WHITE);
		this.setSize(800, 800);

	}
		//Main Method
	public static void main(String s[]) 
	{

		JFrame frame = new JFrame("Type of Triangle ");
		JApplet applet = new Traingle();

		frame.getContentPane().add("Center", applet);
		applet.init();
		frame.setSize(1000, 500);
		frame.setVisible(false);

	}
}
